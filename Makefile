# Quick Makefile to:
# - compile easily a LaTeX-powered research report (make pdf)
# - run easily a Python script, while keeping a text log of its full output (make nicetest)
# - install the requirements (make install)

default: nicetest

run:
	( time nice -n 19 python3 -W ignore::DeprecationWarning -u ./TP_Brain_Imaging__Lilian_Besson__03-2016.py ) | tee ./TP_Brain_Imaging__Lilian_Besson__03-2016.output.txt ; notify-send "./TP_Brain_Imaging__Lilian_Besson__03-2016.py done"

nicetest:
	nice -n 19 python3 -W ignore::DeprecationWarning -u ./TP_Brain_Imaging__Lilian_Besson__03-2016.py | tee ./TP_Brain_Imaging__Lilian_Besson__03-2016.output.txt

# Compilation requires my script autotex : https://bitbucket.org/lbesson/bin/src/master/autotex
# And the file template.en.tex in your ~ : http://perso.crans.org/besson/publis/latex/template.en.tex
pdf:
	+autotex TP_Brain_Imaging__Lilian_Besson__03-2016.en.tex

evince:
	-evince TP_Brain_Imaging__Lilian_Besson__03-2016.en.pdf &

clean:
	rm -rfv *.fls *.fdb_latexmk *.ps *.dvi *.htoc *.tms *.tid *.lg *.log *.id[vx] *.vrb *.toc *.snm *.nav *.htmp *.aux *.tmp *.out *.haux *.hidx *.bbl *.blg *.brf *.lof *.ilg *.ind *.meta *.fdb_latexmk *.fls *.synctex.gz*

compress:
	-PDFCompress TP_Brain_Imaging__Lilian_Besson__03-2016.en.pdf

pylint:
	pylint -d redefined-outer-name TD11__Convergence_of_the_Spacenet_classifier.py

install:
	pip3 install -r requirements.txt

pipinstall:
	pip3 install -r requirements.txt

checkpng:
	for i in "$(grep -o "fig/.*.png" TD11*.tex | tr -d '\\')"; do echo -e "${red}${i}$white"; ll "$i"; done
