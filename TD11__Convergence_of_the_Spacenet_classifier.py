#! /usr/bin/env python3 -W ignore::DeprecationWarning
# -*- coding: utf-8; mode: python -*-
""" TP Brain Imaging : Convergence of the Spacenet classifier (Python 2.7/3.4 script).

Decoding with SpaceNet: face vs house object recognition.

Here is a simple example of decoding with a SpaceNet prior (i.e Graph-Net, TV-L1, etc.), reproducing the Haxby 2001 study on a face vs house discrimination task.


- See also the SpaceNet documentation: https://nilearn.github.io/decoding/space_net.html#haxby.
- This script was originally from: https://nilearn.github.io/auto_examples/02_decoding/plot_haxby_space_net.html.
- Also at: https://github.com/nilearn/nilearn/blob/master/examples/02_decoding/plot_haxby_space_net.py


- *Date:* Sunday 20 March 2016.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility if needed
from sys import version
PYTHON_VERSION = int(version[0])
from time import time
start_time = time()
print("Launching TD11__Convergence_of_the_Spacenet_classifier.py :")
print("Inspired from https://nilearn.github.io/auto_examples/02_decoding/plot_haxby_space_net.html.")

# Trying to remove the DeprecationWarning cause by /home/b/besson/miniconda3/lib/python3.5/site-packages/sklearn/externals/joblib/hashing.py:197
from warnings import filterwarnings  # https://stackoverflow.com/a/879249/5889533
filterwarnings("ignore", category=DeprecationWarning)
filterwarnings("ignore", category=Warning)
# It fails... And calling 'python3 -W ignore::DeprecationWarning' fails too...

# Usual modules
import numpy as np
print("  Imported {}, version {} ...".format("np", np.__version__))
import matplotlib
import matplotlib.pyplot as plt
print("  Imported {}, version {} ...".format("matplotlib", matplotlib.__version__))
import nilearn
print("  Imported {}, version {} ...".format("nilearn", nilearn.__version__))

# nilearn functions
print("  Importing useful functions from nilearn sub-modules: decoding, datasets, image, and plotting ...")
from nilearn.datasets import fetch_haxby
from nilearn.decoding import SpaceNetClassifier
from nilearn.image import index_img
from nilearn.image import mean_img
from nilearn.plotting import plot_stat_map, show

print("> Timing: {:.3g} seconds to load all the modules ...".format(time() - start_time))


def prepareData(labels, target1=b'face', target2=b'house',
                nb_chunks_train=7, verbose=1):
    """ Extract train,test data and background_img from data labels."""
    def log(*args):
        """ Print only if verbose > 0."""
        if verbose > 0:
            print(*args)
    start_time = time()
    # Restrict to face and house conditions
    log("    Shape of labels =", np.shape(labels), "...")
    target = labels['labels']
    log("    Shape of target =", np.shape(target), "...")
    print("\nFocusing on the {} vs {} classification task ...".format(target1, target2))
    condition_mask = np.logical_or(target == target1, target == target2)

    # Split data into train and test samples, using the chunks
    log("  Split data into train and test samples, using the chunks <= or > to {} ...".format(nb_chunks_train))
    condition_mask_train = np.logical_and(condition_mask,
                                          labels['chunks'] <= nb_chunks_train)
    condition_mask_test = np.logical_and(condition_mask,
                                         labels['chunks'] > nb_chunks_train)
    log("> Timing: {:.3g} seconds to prepare the masks ...".format(time() - start_time))

    start_time = time()
    # Apply this sample mask to X (fMRI data) and y (behavioral labels)
    # Because the data is in one single large 4D image, we need to use
    # index_img to do the split easily
    func_filenames = data_files.func[0]

    log("  Applying the mask to X (fMRI data) and y (behavioral labels) ...")
    X_train = index_img(func_filenames, condition_mask_train)
    log("    Shape of X_train =", np.shape(X_train), "...")
    X_test = index_img(func_filenames, condition_mask_test)
    log("    Shape of X_test =", np.shape(X_test), "...")
    y_train = target[condition_mask_train]
    log("    Shape of y_train =", np.shape(y_train), "...")
    y_test = target[condition_mask_test]
    log("    Shape of y_test =", np.shape(y_test), "...")

    # Compute the mean epi to be used for the background of the plotting
    background_img = mean_img(func_filenames)
    log("    Shape of background_img =", np.shape(background_img), "...")

    log("> Timing: {:.3g} seconds to create train/test data and the background_img ...".format(time() - start_time))

    return X_train, X_test, y_train, y_test, background_img
    # Done for prepareData()


def oneExperiment(X_train, X_test, y_train, y_test, background_img,
                  target1=b'face', target2=b'house',
                  showPlot=True, tol=1e-4, verbose=1,
                  use_graph_net=True, use_tv_l1=False):
    """ Run the main experiment, on the classification task target1 vs target2.

    - By default, it reproduces the original Haxby experiment of face vs house classification,
    - But we should also try it with the chair vs scissors.
    """
    def log(*args):
        """ Print only if verbose > 0."""
        if verbose > 0:
            print(*args)
    print("\nCalling the oneExperiment function ...")
    if PYTHON_VERSION >= 3:
        log("  Checking if target1 and target2 are bytes and not string ...")
    if not isinstance(target1, bytes):
        print("[WARNING] In Python 3, target1 should be bytes NOT string!")
        target1 = bytes(target1, encoding='utf-8')
    if not isinstance(target2, bytes):
        print("[WARNING] In Python 3, target2 should be bytes NOT string!")
        target2 = bytes(target2, encoding='utf-8')
    log("  target1 = {}, target2 = {} ...".format(target1, target2))
    start_time = time()
    ##############################################################################
    # Fit SpaceNet with a Graph-Net penalty and TV-L1 penalty
    log("  Fit SpaceNet with a Graph-Net penalty and TV-L1 penalty ...")
    accuracy_graph, accuracy_tv = -1, -1

    if use_graph_net:
        start_time = time()
        # Fit model on train data and predict on test data on the 'graph-net' penalty
        log("\n- Create model on train data and predict on test data on the 'graph-net' penalty ...")
        log("    Using tol =", tol, "...")
        decoder = SpaceNetClassifier(n_jobs=1, memory="/tmp/nilearn_cache",
                                     penalty='graph-net', tol=tol, verbose=1)
        log("    decoder =", decoder)

        log("  Fitting the decoder on X_train and y_train ... (it can take a while) ...")
        decoder.fit(X_train, y_train)
        log("> Timing: {:.3g} seconds to fit this decoder with X_train, y_train ...".format(time() - start_time))

        start_time = time()
        log("  Predicting with this trained decoder on X_test ... (it can take a while) ...")
        y_pred = decoder.predict(X_test)
        accuracy = 100.0 * np.mean(y_pred == y_test)
        print("==> Graph-net classification accuracy : {:.6g}%".format(accuracy))
        accuracy_graph = accuracy
        log("> Timing: {:.3g} seconds to predict for X_test with this decoder ...".format(time() - start_time))

        start_time = time()
        # Visualization
        log("  Visualizing the result ...")
        log("  Reading the coef_img_ attribute of the decoder (like support vectors) ...")
        coef_img = decoder.coef_img_
        display = plot_stat_map(coef_img, background_img,
                                title="Graph-Net: accuracy {:.4g}%, tol {:.3g}".format(accuracy, tol),
                                cut_coords=(-34, -16), display_mode="yz")
        # Save the coefficients to a nifti file
        log("  Saving the coefficients to a nifti file 'niidata/haxby_graph-net_weights.nii' ...")
        coef_img.to_filename('niidata/haxby_graph-net_weights.nii')
        if showPlot:
            log("  Showing the image ...")
            show()
        output_file = "fig/Haxby__{}_vs_{}__Graph-Net__accuracy_{:.6g}__tol_{:.3g}".format(target1.decode(), target2.decode(), accuracy, tol)
        output_file = output_file.replace('.', '_') + ".png"
        print("  Saving the image to '{}' ...".format(output_file))
        # http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.savefig
        try:
            display.savefig(output_file, dpi=400)
        except Exception as e:
            print("Exception:", e)
            log("    Failing to use dpi=400, using default: display.save(output_file) ...")
            display.savefig(output_file)
        display.close()
        log("> Timing: {:.3g} seconds to generate and save the image ...".format(time() - start_time))

    ##############################################################################
    if use_tv_l1:
        # Now Fit SpaceNet with a TV-l1 penalty
        start_time = time()
        log("\n- Create model on train data and predict on test data on the 'tv-l1' penalty ...")
        log("    Using tol =", tol, "...")
        decoder = SpaceNetClassifier(n_jobs=1, memory="/tmp/nilearn_cache",
                                     penalty='tv-l1', tol=tol, verbose=1)
        log("    decoder =", decoder)

        log("  Fitting the decoder on X_train and y_train ... (it can take a while) ...")
        decoder.fit(X_train, y_train)
        log("> Timing: {:.3g} seconds to fit this decoder with X_train, y_train ...".format(time() - start_time))

        start_time = time()
        log("  Predicting with this trained decoder on X_test ... (it can take a while) ...")
        y_pred = decoder.predict(X_test)
        accuracy = 100.0 * np.mean(y_pred == y_test)
        print("==> TV-l1 classification accuracy : {:.6g}%".format(accuracy))
        accuracy_tv = accuracy
        log("> Timing: {:.3g} seconds to predict for X_test with this decoder ...".format(time() - start_time))

        start_time = time()
        # Visualization
        log("  Visualizing the result ...")
        log("  Reading the coef_img_ attribute of the decoder (like support vectors) ...")
        coef_img = decoder.coef_img_
        display = plot_stat_map(coef_img, background_img,
                                title="TV-L1: accuracy {:.4g}%, tol {:.3g}".format(accuracy, tol),
                                cut_coords=(-34, -16), display_mode="yz")
        # Save the coefficients to a nifti file
        log("  Saving the coefficients to a nifti file 'niidata/haxby_tv-l1_weights.nii' ...")
        coef_img.to_filename('niidata/haxby_tv-l1_weights.nii')
        if showPlot:
            log("  Showing the image ...")
            show()
        output_file = "fig/Haxby__{}_vs_{}__TV-L1__accuracy_{:.6g}__tol_{:.3g}".format(target1.decode(), target2.decode(), accuracy, tol)
        output_file = output_file.replace('.', '_') + ".png"
        print("  Saving the image to '{}' ...".format(output_file))
        # http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.savefig
        try:
            display.savefig(output_file, dpi=400)
        except Exception as e:
            print("Exception:", e)
            log("    Failing to use dpi=400, using default: display.save(output_file) ...")
            display.savefig(output_file)
        display.close()
        log("> Timing: {:.3g} seconds to generate and save the image ...".format(time() - start_time))

    return (accuracy_graph, accuracy_tv)
    # Done for oneExperiment()


def experiment1(X_train, X_test, y_train, y_test, background_img,
                targets_to_try, showPlot=True):
    """ First experiment, trying every classification experiment of the target1 vs target2 found in targets_to_try.
    """
    N = len(targets_to_try)
    deltats = np.zeros(N)
    for i, (target1, target2) in enumerate(targets_to_try):
        start_time = time()
        print("\n\nStarting the first experiment with target1 = {}, target2 = {} ...".format(target1, target2))
        oneExperiment(X_train, X_test, y_train, y_test, background_img,
                      target1, target2, verbose=1,
                      showPlot=showPlot, use_graph_net=True, use_tv_l1=True)
        print("\nDone for this experiment with target1 = {}, target2 = {} ...".format(target1, target2))
        print("> Timing: {:.3g} seconds for this experiment ...".format(time() - start_time))
        deltats[i] = time() - start_time
    return deltats
    # Done for experiment1()


def experiment2(X_train, X_test, y_train, y_test, background_img,
                grid_tol=[1e-4], target1=b'face', target2=b'house',
                deltat=900, showPlot=False):
    """ Second experiment, trying the target1 vs target2 experiment, with every tolerance parameter found in grid_tol.
    """
    print("\n\nStarting the main experiment with target1 = {}, target2 = {} ...".format(target1, target2))
    print("Exploring the grid of tolerance parameters tol:\ngrid_tol =", grid_tol)
    N = len(grid_tol)

    print("There is N = {} tol values to try, each test takes about {:.3g} seconds ...\nSo the total running time should be about {:.4g} minutes ...".format(N, deltat, N*deltat/60))
    times = np.zeros(N)
    accuracies_graph = np.zeros(N)
    accuracies_tv = np.zeros(N)

    for i in range(N):
        tol = grid_tol[i]
        print("\n+ Experiment # {} / {}, and tol = {}, calling oneExperiment ...".format(i+1, N, tol))
        start_time = time()
        accuracy_graph, accuracy_tv = oneExperiment(X_train, X_test, y_train, y_test, background_img,
                                                    target1, target2,
                                                    tol=tol, verbose=1, showPlot=False,
                                                    use_graph_net=True, use_tv_l1=False)
        times[i] = time() - start_time
        accuracies_graph[i] = accuracy_graph
        accuracies_tv[i] = accuracy_tv
        print("For i = {} and tol = {}, oneExperiment ran for {:.4g} minutes, with accuracies: Graph-Net = {:.3g}%, and TV-L1 = {:.3g}% ...".format(i, tol, times[i]/60, accuracy_graph, accuracy_tv))
    # Plot results
    print("\n\nDone for the N = {} experiments, now plotting the results: CPU time, and accuracy, as functions tolerance parameter tol ...".format(N))
    for accuracies, accuname in zip([accuracies_tv, accuracies_graph],
                                    ["TV-L1", "Graph-Net"]):
        print("Accuracy {}: accuracies = {} ...".format(accuname, accuracies))
        # If one value is < 0, it means it's a -1, the experiment did not happen
        if np.all(accuracies >= 0):
            for y, ylabel, color in zip([times, accuracies],
                                        ["Computation time", "Accuracy"],
                                        ['r', 'g']):
                ylabel = ylabel + " with " + accuname
                print("Plotting for", ylabel, "as function of tolerance parameter tol ...")
                plt.figure()
                plt.title("{} as function of tolerance parameter tol".format(ylabel))
                plt.xlabel("Tolerance parameter tol = {} (-log10(tol))".format(grid_tol))
                plt.ylabel(ylabel)
                x = -np.log10(grid_tol)
                plt.plot(x, y, color+'*-', lw=3, ms=16)
                if showPlot:
                    plt.show()
                outname = "fig/Plot_{}__N={}__{}_vs_{}.png".format(ylabel.replace(' ', '_'), N, target1.decode(), target2.decode())
                print("Saving to", outname, "...")
                plt.savefig(outname, dpi=160)
    # Done for experiment2()


##############################################################################
if __name__ == '__main__':
    start_time = time()
    # Load the Haxby dataset
    print("  Downloading the Haxby databases... will take a while ... ")
    data_files = fetch_haxby()
    print("> Timing: {:.3g} seconds to load all the Haxby databases ...".format(time() - start_time))

    start_time = time()
    # Print basic information on the dataset
    print("  - First subject anatomical nifti image (3D) is at: {} ...".format(data_files.anat[0]))  # 3D data
    print("  - First subject functional nifti images (4D) are at: {} ...".format(data_files.func[0]))  # 4D data
    # print("  - data_files: {} ...".format(data_files))

    # Load Target labels
    print("  Loading the target labels ...")
    labels = np.recfromcsv(data_files.session_target[0], delimiter=' ')
    print("  Shape of labels =", np.shape(labels), "...")
    print("> Timing: {:.3g} seconds to load the target labels ...".format(time() - start_time))

    list_of_labels = np.unique(labels['labels'])
    # list_of_str_labels = ['bottle', 'cat', 'chair', 'face', 'house', 'rest', 'scissors', 'scrambledpix', 'shoe']
    list_of_str_labels = [b.decode() for b in list_of_labels]
    print("  The different labels from the Haxby database are {} ...".format(list_of_str_labels))

    # Do it only once, it takes about 25 seconds!
    X_train, X_test, y_train, y_test, background_img = prepareData(labels)

    # True to visualize the image generated by experiment1, False to disable them
    showPlot = True
    showPlot = False

    # 1.
    EXPERIMENT1 = True
    EXPERIMENT1 = False
    # True to enable experiment1, False to disable it
    if EXPERIMENT1:
        # List of experiment to try
        targets_to_try = [
            (b'face', b'house'),     # "Easy"
            (b'cat', b'house'),      # "Easy"
            (b'scissors', b'chair')  # "Hard"
        ]
        for (target1, target2) in targets_to_try:
            assert target1 in list_of_labels, "[ERROR] target1 = {} is NOT a valid label !".format(target1)
            assert target2 in list_of_labels, "[ERROR] target2 = {} is NOT a valid label !".format(target2)
        print("  targets_to_try = {} ...".format(targets_to_try))

        print("\n\n1. Starting the first experiment.")
        deltats = experiment1(X_train, X_test, y_train, y_test, background_img,
                              targets_to_try, showPlot=showPlot)
        print("==> Done for this first experiment.")
        # Done
        print("\n==> We can see that the TV-l1 penalty is 3 times slower to converge and gives the same prediction accuracy.")
        print("==> However, it yields much cleaner coefficient maps.")
    else:
        deltats = [900]  # ~= 15 minutes
    deltat = np.mean(deltats)

    # 2.
    EXPERIMENT2 = False
    EXPERIMENT2 = True
    # True to enable experiment2, False to disable it
    if EXPERIMENT2:
        target1, target2 = b'face', b'house'
        grid_tol = np.logspace(1, -8, 17)
        grid_tol = [1]  # To quickly test the function experiment2()
        grid_tol = [1, 1e-1, 1e-3, 1e-5]
        grid_tol = [1, 1e-1, 1e-2]
        # grid_tol = [10**i for i in range(0, -6, -1)]
        grid_tol = [1, 0.5, 0.1, 0.05, 0.01, 0.005, 0.001, 0.0001, 1e-05]
        print("grid_tol =", grid_tol)
        print("\n\n2. Starting the second experiment.")
        experiment2(X_train, X_test, y_train, y_test, background_img,
                    grid_tol, target1, target2, deltat=deltat, showPlot=showPlot)
        print("==> Done for this second experiment.")

# End of TD11__Convergence_of_the_Spacenet_classifier.py
