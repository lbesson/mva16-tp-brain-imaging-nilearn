# -*- coding: utf-8 -*-
"""
http://nilearn.github.io/decoding/decoding_tutorial.html
"""

from nilearn import datasets

print("Downloading the Haxby databases... will take a while ... ")
haxby_dataset = datasets.fetch_haxby()

# print basic information on the dataset
print("First subject anatomical nifti image (3D) is at: %s" %
      haxby_dataset.anat[0])
print("First subject functional nifti images (4D) are at: %s" %
      haxby_dataset.func[0])  # 4D data

print(haxby_dataset)


import numpy as np
# Load target information as string and give a numerical identifier to each
labels = np.recfromcsv(haxby_dataset.session_target[0], delimiter=" ")

# scikit-learn >= 0.14 supports text labels. You can replace this line by:
# target = labels['labels']
_, target = np.unique(labels['labels'], return_inverse=True)

n_conditions = len(set(target))


# Keep only data corresponding to faces or cats
condition_mask = np.logical_or(labels['labels'] == b'face',
                               labels['labels'] == b'cat')

target = y = target[condition_mask]


###########################################################################


from nilearn.input_data import NiftiMasker
mask_filename = haxby_dataset.mask_vt[0]
# For decoding, standardizing is often very important
nifti_masker = NiftiMasker(mask_img=mask_filename, standardize=True)

func_filename = haxby_dataset.func[0]
# We give the nifti_masker a filename and retrieve a 2D array ready
# for machine learning with scikit-learn
fmri_masked = nifti_masker.fit_transform(func_filename)

# Restrict the classification to the face vs cat discrimination
fmri_masked = X = fmri_masked[condition_mask]

###########################################################################

from sklearn.svm import SVC
svc = SVC(kernel='linear')

print("SVM : using a SVC with linear kernel.")
print(svc)

svc.fit(fmri_masked, target)
prediction = svc.predict(fmri_masked)

print("Confusion matrix :")
from sklearn.metrics import confusion_matrix
print(confusion_matrix(target, prediction))

###########################################################################

from sklearn.cross_validation import KFold

print("Using a n_folds = 5 KFold cross_validation ...")
cv = KFold(n=len(fmri_masked), n_folds=5)
cv_scores = []

for train, test in cv:
    svc.fit(fmri_masked[train], target[train])
    prediction = svc.predict(fmri_masked[test])
    cv_scores.append(np.sum(prediction == target[test])
                     / float(np.size(target[test])))


print("Using cross_val_score from cross_validation ...")
from sklearn.cross_validation import cross_val_score
cv_scores = cross_val_score(svc, fmri_masked, target, cv=cv)

cv_scores = cross_val_score(svc, fmri_masked, target, cv=cv, n_jobs=-1, verbose=10)

print("Scores, mean and std of scores ...")
print(cv_scores)
print(np.mean(cv_scores))
print(np.std(cv_scores))


print("Using a LeaveOneLabelOut cross validation tool ...")
from sklearn.cross_validation import LeaveOneLabelOut
session_label = labels['chunks']
# We need to remember to remove the rest conditions
session_label = session_label[condition_mask]
cv = LeaveOneLabelOut(labels=session_label)

# Default scoring
print("Using default scoring ...")
cv_scores = cross_val_score(svc, fmri_masked, target, cv=cv)
print(cv_scores)
classification_accuracy = np.mean(cv_scores)
print(np.mean(cv_scores))
print(np.std(cv_scores))

# Scoring F1
print("Using another scoring : f1-score ...")
cv_scores = cross_val_score(svc, fmri_masked, target, cv=cv,  scoring='f1')
print(cv_scores)
classification_accuracy = np.mean(cv_scores)
print(np.mean(cv_scores))
print(np.std(cv_scores))


# DummyClassifier
print("Using DummyClassifier ...")
from sklearn.dummy import DummyClassifier
null_cv_scores = cross_val_score(DummyClassifier(), fmri_masked, target, cv=cv)
print(null_cv_scores)

print("And using permutation_test_score...")
from sklearn.cross_validation import permutation_test_score
null_cv_scores = permutation_test_score(svc, fmri_masked, target, cv=cv)
print(null_cv_scores)


# Retrieve the SVC discriminating weights
coef_ = svc.coef_

# Reverse masking thanks to the Nifti Masker
coef_img = nifti_masker.inverse_transform(coef_)

# Save the coefficients as a Nifti image
coef_img.to_filename('haxby_svc_weights.nii')

###########################################################################

print("Using a linear SVC and then a SelectKBest with k=500 ...")
# Define the prediction function to be used.
# Here we use a Support Vector Classification, with a linear kernel
from sklearn.svm import SVC
svc = SVC(kernel='linear')
print(svc)

# Define the dimension reduction to be used.
# Here we use a classical univariate feature selection based on F-test,
# namely Anova. We set the number of features to be selected to 500
from sklearn.feature_selection import SelectKBest, f_classif
feature_selection = SelectKBest(f_classif, k=500)
print(feature_selection)

# We have our classifier (SVC), our feature selection (SelectKBest), and now,
# we can plug them together in a *pipeline* that performs the two operations
# successively:
from sklearn.pipeline import Pipeline
anova_svc = Pipeline([('anova', feature_selection), ('svc', svc)])
print(anova_svc)

#############################################################################
# Fit the decoder and predict

anova_svc.fit(X, y)
y_pred = anova_svc.predict(X)
print("   Fix the typo : do a PR on GitHub ...")



# Default scoring
print("Using default scoring ...")
cv_scores = cross_val_score(anova_svc, fmri_masked, target, cv=cv)
print(cv_scores)
classification_accuracy = np.mean(cv_scores)
print(np.mean(cv_scores))
print(np.std(cv_scores))

# Scoring F1
print("Using another scoring : f1-score ...")
cv_scores = cross_val_score(anova_svc, fmri_masked, target, cv=cv,  scoring='f1')
print(cv_scores)
classification_accuracy = np.mean(cv_scores)
print(np.mean(cv_scores))
print(np.std(cv_scores))

#############################################################################

# Look at the SVC's discriminating weights
coef = svc.coef_
# reverse feature selection
coef = feature_selection.inverse_transform(coef)
# reverse masking
weight_img = nifti_masker.inverse_transform(coef)


# Create the figure
from nilearn import image
from nilearn.plotting import plot_stat_map, show

# Plot the mean image because we have no anatomic data
mean_img = image.mean_img(func_filename)

plot_stat_map(weight_img, mean_img, title='SVM weights')

# Saving the results as a Nifti file may also be important
weight_img.to_filename('haxby_face_vs_house.nii')

#############################################################################

print("Fisher’s Linear Discriminant Analysis (LDA)")

from sklearn.lda import LDA
from sklearn.pipeline import Pipeline
lda = LDA()
anova_lda = Pipeline([('anova', feature_selection), ('LDA', lda)])

cv_scores = cross_val_score(anova_lda, X, y, cv=cv, verbose=1)
classification_accuracy = np.mean(cv_scores)
print("Classification accuracy: %.4f / Chance Level: %.4f" % \
   (classification_accuracy, 1. / n_conditions))

#############################################################################
from sklearn.feature_selection import RFE

rfe = RFE(SVC(kernel='linear', C=1.), 50, step=0.25)

rfe_svc = Pipeline([('rfe', rfe), ('svc', clf)])

cv_scores = cross_val_score(rfe_svc, X, y, cv=cv, n_jobs=-1,
    verbose=1)

